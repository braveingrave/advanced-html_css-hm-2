const { series, src, dest, watch, parallel } = require('gulp');
const browserSync = require('browser-sync').create();
const clean = require('gulp-clean');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const imgmin = require('gulp-imagemin');
const minifyjs = require('gulp-js-minify');
const concat = require('gulp-concat');

function buildJS(){
  return src('./src/*.js')
    .pipe(concat('index.js'))
    .pipe(minifyjs())
    .pipe(dest('./dist/js'))
}

function cleanDist(cb){
  src('dist/*').pipe(clean())
  cb();
}

function buildCSS() {
  return src('./src/scss/style.scss')
  .pipe(sass().on('error', sass.logError))
  .pipe(cleanCSS())
  .pipe(autoprefixer())
  .pipe(dest('dist/css'))
}

function imgM(){
  return src('src/img/*')
		.pipe(imgmin())
		.pipe(dest('dist/img'))
}

function serve() {
    browserSync.init({
      server: {
        baseDir: "./"
      }
    });
    watch("./src/scss/*.scss", (cb) => { 
      buildCSS(); 
      browserSync.reload();
      cb();
    });
    watch("./src/*.js", (cb) => {
      buildJS();
      browserSync.reload();
      cb();
      });
    watch("./src/*.html").on('change', browserSync.reload);
  }
exports.buildCSS = buildCSS;
exports.build = series(cleanDist, parallel(buildCSS, imgM, buildJS,));
exports.dev = series(cleanDist, parallel(buildCSS, imgM, buildJS), serve);
exports.default = serve;
